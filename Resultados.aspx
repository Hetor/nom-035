﻿<%@ Page Title="REULTADOS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Resultados.aspx.cs" Inherits="NOM_035.Resultados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row divmsgBox">
        <div id="diverror" runat="server" class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Oh no 😱! </strong>
            <asp:Label ID="lblerror" runat="server" Text="Hectorw/h"></asp:Label>
        </div>
    </div>
    <div id="divdatos" class="row">
        <asp:Panel ID="AreaPreguntas" runat="server" Font-Size="XX-Small"></asp:Panel>

        <asp:Panel ID="AreaRespuestas" runat="server" Font-Size="XX-Small"></asp:Panel>

        <table class="table table-bordered table-condensed" style="font-size: xx-small">
            <thead>
                <tr>
                    <th style="background: #eeeeee;">Categoría</>
                    <th style="background: #eeeeee;">Dominio</th>
                    <th style="background: #eeeeee;">Dimensión</th>
                    <th style="background: #eeeeee;">ítem</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td rowspan="3">Ambiente de trabajo</td>
                    <td rowspan="3">Condiciones en el ambiente de trabajo</td>
                    <td>Condiciones peligrosas e inseguras</td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Condiciones deficientes e insalubres</td>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Trabajos peligrosos</td>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="10">Factores propios de la actividad</td>
                    <td rowspan="6">Carga de trabajo</td>
                    <td>Cargas cuantitativas</td>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Ritmos de trabajo acelerado</td>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Carga mental</td>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Cargas psicológicas emocionales</td>
                    <td>
                        <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Cargas de alta responsabilidad</td>
                    <td>
                        <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Cargas contradictorias o inconsistentes</td>
                    <td>
                        <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="4">Falta de control sobre el trabajo</td>
                    <td>Falta de control y autonomía sobre el trabajo</td>
                    <td>
                        <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Limitada o nula posibilidad de desarrollo</td>
                    <td>
                        <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Insuficiente participación y manejo del cambio</td>
                    <td>
                        <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Limitada o inexistente capacitación</td>
                    <td>
                        <asp:Label ID="Label13" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="3">Organización del tiempo de trabajo</td>
                    <td>Jornada de trabajo</td>
                    <td>Jornadas de trabajo extensas</td>
                    <td>
                        <asp:Label ID="Label14" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="2">Interferencia en la relación trabajo-familia</td>
                    <td>Influencia del trabajo fuera del centro laboral</td>
                    <td>
                        <asp:Label ID="Label15" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Influencia de las responsabilidades familiares </td>
                    <td>
                        <asp:Label ID="Label16" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="5">Liderazgo y relaciones en el trabajo</td>
                    <td rowspan="2">Liderazgo</td>
                    <td>Escaza claridad de funciones</td>
                    <td>
                        <asp:Label ID="Label17" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Características del liderazgo</td>
                    <td>
                        <asp:Label ID="Label18" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="2">Relaciones en el trabajo</td>
                    <td>Relaciones sociales en el trabajo</td>
                    <td>
                        <asp:Label ID="Label19" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Deficiente relación con los colaboradores que supervisa</td>
                    <td>
                        <asp:Label ID="Label20" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Violencia</td>
                    <td>Violencia laboral</td>
                    <td>
                        <asp:Label ID="Label21" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="4">Entorno organizacional</td>
                    <td rowspan="2">Reconocimiento del desempeño</td>
                    <td>Escasa o nula retroalimentación del desempeño</td>
                    <td>
                        <asp:Label ID="Label22" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Escaso o nulo reconocimiento y compensación</td>
                    <td>
                        <asp:Label ID="Label23" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td rowspan="2">Insuficiente sentido de pertenencia e, inestabilidad</td>
                    <td>Limitado sentido de pertenencia</td>
                    <td>
                        <asp:Label ID="Label24" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td>Inestabilidad laboral</td>
                    <td>
                        <asp:Label ID="Label25" runat="server" Text="Label"></asp:Label></td>
                </tr>
            </tbody>
        </table>

        <div class="col-lg-4 col-md-4 col-xs-4">
            <br />
            <br />
            <hr style="border: 1px solid black" />
            <p style="text-align: center">
                <asp:Label ID="Nombre" runat="server" Text="HECTOR w/h"></asp:Label></p>
            <p style="text-align: center">Firma</p>
        </div>
    </div>
</asp:Content>
