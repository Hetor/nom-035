﻿<%@ Page Title="RESULTADO" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Empleados.aspx.cs" Inherits="NOM_035.Empleados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <h1>
                <%: Title %>
            </h1>
            <div id="diverror" runat="server" class="alert alert-danger" style="margin-top: 5px;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Oh snap! </strong>
                <asp:Label ID="lblerror" runat="server" Text="Hectorw/h"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-xs-4">
            <div style="margin-bottom: 15px;">
                <asp:Label ID="Label1" runat="server" AssociatedControlID="EmpresaDrop">EMPRESA</asp:Label>
                <asp:DropDownList ID="EmpresaDrop" CssClass="form-control text-uppercase" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
                    <asp:ListItem Text="HUNGAROS" Value="HUNGAROS" />
                    <asp:ListItem Text="4 MATILDES" Value="4 MATILDES" />
                    <asp:ListItem Text="JAMAMADI" Value="JAMAMADI" />
                </asp:DropDownList>
            </div>
            <div style="margin-bottom: 15px;">
                <asp:Label ID="Label2" runat="server" AssociatedControlID="txtNombre">NOMBRE</asp:Label>
                <div class="input-group">
                    <asp:TextBox runat="server" ID="txtNombre" AutoPostBack="false" CssClass="form-control text-uppercase" Style="width: 280px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNombre"
                        CssClass="alert alert-danger" Style="padding: 5px; margin: 0px 15px; position: fixed;"
                        ErrorMessage="Nombre Requerido!" />
                </div>
            </div>
            <div style="margin-bottom: 15px;">
                <asp:Label ID="Label3" runat="server" AssociatedControlID="txtApellido">APELLIDO</asp:Label>
                <div class="input-group">
                    <asp:TextBox runat="server" ID="txtApellido" AutoPostBack="false" CssClass="form-control text-uppercase" Style="width: 280px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtApellido"
                        CssClass="alert alert-danger" Style="padding: 5px; margin: 0px 15px; position: fixed;"
                        ErrorMessage="Apellido Requerido!" />
                </div>
            </div>
            <div class="nav navbar-left" style="margin-right: 20px;">
                <asp:Button ID="btnCancelar" runat="server" CssClass="btn btn-danger" Text="Cancel" CausesValidation="False" OnClick="BtnCancelar_Click" />
                <asp:Button ID="btnIniciar" runat="server" CssClass="btn btn-primary" Text="Guardar" OnClick="BtnIniciar_Click" />
            </div>
        </div>

        <div class="col-lg-8 col-md-8 col-xs-8" style="margin-top: 17px;">
            <div>
                <table class="table">
                    <tr>
                        <td style="padding-right: 0px; padding-left: 0px; border-top: 0px;">
                            <asp:TextBox ID="txtsearch" runat="server" OnTextChanged="Serch_OnTextChanged" AutoPostBack="true" CssClass="fill form-control text-uppercase" Style="border-top-right-radius: 0px; border-bottom-right-radius: 0px;"></asp:TextBox>
                        </td>
                        <td style="padding-right: 0px; padding-left: 0px; border-top: 0px; width: 70px;">
                            <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-warning" Text="Buscar" OnClick="BtnBuscar_Click" Style="border-top-left-radius: 0px; border-bottom-left-radius: 0px;" CausesValidation="False" />
                        </td>
                    </tr>

                </table>
            </div>
            <div>
                <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-condensed table-hover table-responsive" AutoPostBack="false"
                    AutoGenerateColumns="False" GridLines="None" HeaderStyle-CssClass="headert" AllowSorting="True" HeaderStyle-ForeColor="Black" OnRowCommand="GridView1_RowCommand">
                    <Columns>
                        <asp:BoundField HeaderText="id" DataField="id" HeaderStyle-Width="25px" />
                        <asp:BoundField HeaderText="EMPRESA" DataField="empresa" HeaderStyle-Width="25px" />
                        <asp:BoundField HeaderText="NOMBRE" DataField="nombre" />
                        <asp:BoundField HeaderText="APELLIDO" DataField="apellido" />
                        <asp:ButtonField ButtonType="Image" ImageUrl="~/Content/icon_m_edit.png" CommandName="Editar" Text="Editar" HeaderStyle-Width="25px" ControlStyle-Width="25px" />
                        <asp:ButtonField ButtonType="Image" ImageUrl="~/Content/icon_m_respuestas.png" CommandName="Respuestas" Text="Respuestas" HeaderStyle-Width="25px" ControlStyle-Width="25px" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
