﻿using NOM_035.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace NOM_035
{
    public partial class _Default : Page
    {
        private readonly Usuarios u = new Usuarios();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtNombre.Focus();

                diverror.Visible = false;
            }
        }

        #region------------------------------BOTONES------------------------------
        protected void BtnIniciar_Click(object sender, EventArgs e)
        {
            Agregar();
        }

        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                txtNombre.Text = "";
                txtApellido.Text = "";

                txtNombre.Focus();
            }
            catch (Exception)
            {
                //lblerror.Text = Convert.ToString(Exception);
            }
        }
        #endregion

        #region ---------------------------EVENTOS ENTER---------------------------
        protected void Add_OnTextChanged(object sender, EventArgs e)
        {
            Agregar();
        }
        #endregion

        #region------------------------------METODOS------------------------------
        private void Agregar()
        {
            u.Nombre = txtNombre.Text.ToUpper();
            u.Apellido = txtApellido.Text.ToUpper();
            u.Empresa = EmpresaDrop.SelectedValue;

            u.Insert();

            u.GetUltimo();
            Session["ID"] = u.Id;
            Response.Redirect("Cuestionario.aspx?gp=1&us=" + u.Id.ToString());
        }
        #endregion
    }
}