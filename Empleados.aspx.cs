﻿using NOM_035.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NOM_035
{
    public partial class Empleados : System.Web.UI.Page
    {
        int fila;
        private readonly Usuarios u = new Usuarios();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtsearch.Focus();

                diverror.Visible = false;

                GridView1.DataSource = u.GetAll(ref lblerror, ref diverror);
                GridView1.DataBind();
            }
        }

        #region------------------------------BOTONES------------------------------
        protected void BtnBuscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        protected void BtnIniciar_Click(object sender, EventArgs e)
        {
            Actualizar();
        }

        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                //txtsearch.Focus();
                txtsearch.Attributes.Add("onfocus", "this.select();");

                txtNombre.Text = "";
                txtApellido.Text = "";

                txtsearch.Focus();
            }
            catch (Exception)
            {
                //MsgBox("SEARCH TROUBLE");
            }
        }
        #endregion

        #region ---------------------------EVENTOS ENTER---------------------------
        protected void Serch_OnTextChanged(object sender, EventArgs e)
        {
            Buscar();
        }
        #endregion

        #region------------------------------METODOS------------------------------
        private void Buscar()
        {
            u.Searchby = txtsearch.Text;

            GridView1.DataSource = u.Search(ref lblerror, ref diverror);
            GridView1.DataBind();
        }

        private void Actualizar()
        {
            u.Nombre = txtNombre.Text.ToUpper();
            u.Apellido = txtApellido.Text.ToUpper();
            u.Empresa = EmpresaDrop.SelectedValue;
            u.Id = Convert.ToInt32(Session["ID"]);

            u.Update();

            txtApellido.Text = txtNombre.Text = "";

            GridView1.DataSource = u.GetAll(ref lblerror, ref diverror);
            GridView1.DataBind();

            txtsearch.Focus();
        }
        #endregion

        #region-----------------------------GRIDVIEW-----------------------------
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            fila = Convert.ToInt32(e.CommandArgument);

            if (e.CommandName == "Editar")
            {
                Session["ID"] = GridView1.Rows[fila].Cells[0].Text;
                EmpresaDrop.SelectedValue = GridView1.Rows[fila].Cells[1].Text;
                txtNombre.Text = HttpUtility.HtmlDecode(GridView1.Rows[fila].Cells[2].Text);
                txtApellido.Text = HttpUtility.HtmlDecode(GridView1.Rows[fila].Cells[3].Text);
            }
            if (e.CommandName == "Respuestas")
            {
                Session["ID"] = GridView1.Rows[fila].Cells[0].Text;
                Response.Redirect("Resultados.aspx?us=" + GridView1.Rows[fila].Cells[0].Text);
            }
        }
        #endregion
    }
}