﻿using NOM_035.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NOM_035
{
    public partial class Cuestionario : System.Web.UI.Page
    {
        #region variables
        int user = 0;
        int grupo = 1;
        //int respuesta = 0;
        int totalpreguntas = 0;
        List<preguntas> listPreguntas;
        private readonly preguntas p = new preguntas();
        private readonly cuestionario c = new cuestionario();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                diverror.Visible = false;

                if (grupo == 13 /*&& respuesta != 0*/ || grupo == 14 /*&& respuesta != 0*/)
                {
                    if (grupo == 13)
                        lblPregunta.Text = "¿En mi trabajo debo brindar servicio a clientes o usuarios?";
                    if (grupo == 14)
                        lblPregunta.Text = "¿Soy jefe de otros trabajadores?";

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script language='javascript'>");
                    sb.Append(@"$('#myModal').modal('show');");
                    sb.Append(@"</script>");

                    ClientScript.RegisterStartupScript(this.GetType(), "JSScript", sb.ToString());
                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (Request.QueryString["gp"] != null)
            {
                grupo = Convert.ToInt32(Request.QueryString["gp"]);
                user = Convert.ToInt32(Request.QueryString["us"]);
            }

            Llenar();
        }

        public void Llenar()
        {
            p.Searchby = grupo.ToString();
            listPreguntas = p.GetGrupo(ref lblerror, ref diverror);
            totalpreguntas = listPreguntas.Count;

            switch (grupo)
            {
                case 1:
                    H2.Text = "Para responder las preguntas siguientes considere las condiciones ambientales de su centro de trabajo.";
                    break;
                case 2:
                    H2.Text = "Para responder a las preguntas siguientes piense en la cantidad y ritmo de trabajo que tiene.";
                    break;
                case 3:
                    H2.Text = "Las preguntas siguientes están relacionadas con el esfuerzo mental que le exige su trabajo.";
                    break;
                case 4:
                    H2.Text = "Las preguntas siguientes están relacionadas con las actividades que realiza en su trabajo y las responsabilidades que tiene.";
                    break;
                case 5:
                    H2.Text = "Las preguntas siguientes están relacionadas con su jornada de trabajo.";
                    break;
                case 6:
                    H2.Text = "Las preguntas siguientes están relacionadas con las decisiones que puede tomar en su trabajo.";
                    break;
                case 7:
                    H2.Text = "Las preguntas siguientes están relacionadas con cualquier tipo de cambio que ocurra en su trabajo (considere los últimos cambios realizados).";
                    break;
                case 8:
                    H2.Text = "Las preguntas siguientes están relacionadas con la capacitación e información que se le proporciona sobre su trabajo.";
                    break;
                case 9:
                    H2.Text = " Las preguntas siguientes están relacionadas con el o los jefes con quien tiene contacto.";
                    break;
                case 10:
                    H2.Text = "Las preguntas siguientes se refieren a las relaciones con sus compañeros.";
                    break;
                case 11:
                    H2.Text = "Las preguntas siguientes están relacionadas con la información que recibe sobre su rendimiento en el trabajo, el reconocimiento, el sentido de pertenencia y la estabilidad que le ofrece su trabajo.";
                    break;
                case 12:
                    H2.Text = "Las preguntas siguientes están relacionadas con actos de violencia laboral (malos tratos, acoso, hostigamiento, acoso psicológico).";
                    break;
                case 13:
                    H2.Text = "Las preguntas siguientes están relacionadas con la atención a clientes y usuarios.";
                    break;
                case 14:
                    H2.Text = "Las preguntas siguientes están relacionadas con las actitudes de las personas que supervisa.";
                    break;
            }

            for (int i = 0; i < totalpreguntas; i++)
            {
                CreateArea(listPreguntas[i].Id.ToString(), listPreguntas[i].Pregunta);
                if (i == totalpreguntas - 1)
                {
                    //preguntas = 100 / 72; = 1.38888888889
                    double porcentage = 1.38888888889 * listPreguntas[i].Id;
                    porcentage = Math.Round(porcentage);

                    AreaProgress.Controls.Add(new LiteralControl("<div class='progress-bar' role='progressbar' style='width: " + porcentage.ToString() + "%' aria-valuenow='" + porcentage.ToString() + "' aria-valuemin='0' aria-valuemax='100'></div>"));
                }
            }
        }

        private void CreateArea(string areaCount, string pregunta)
        {
            //HTML
            AreaPreguntas.Controls.Add(new LiteralControl("<div style='margin-bottom: 15px;' class='list-group-item list-group-item-action list-group-item-info'>"));
            AreaPreguntas.Controls.Add(new LiteralControl("<div class='input-group'>"));

            //ASP
            Label Labelcito = new Label();
            Labelcito.ID = "Label" + areaCount;
            Labelcito.AssociatedControlID = "RadioButtonList" + areaCount;
            Labelcito.Text = areaCount + "." + pregunta;

            AreaPreguntas.Controls.Add(Labelcito);

            RequiredFieldValidator RFV = new RequiredFieldValidator();
            RFV.ID = "RequiredFieldValidator" + areaCount;
            RFV.ControlToValidate = "RadioButtonList" + areaCount;
            RFV.CssClass = "alert alert-danger";
            RFV.Style.Add("padding", "5px");
            RFV.Style.Add("margin", "0px 15px");
            RFV.ErrorMessage = "Por Favor Responda esta Pregunta!";

            AreaPreguntas.Controls.Add(RFV);

            //HTML
            AreaPreguntas.Controls.Add(new LiteralControl("</div>"));

            //ASP
            RadioButtonList RBL = new RadioButtonList();
            RBL.ID = "RadioButtonList" + areaCount;
            RBL.RepeatDirection = RepeatDirection.Horizontal;
            RBL.Width = 600;
            RBL.Style.Add("margin-left", "20px");
            RBL.CellPadding = 10;
            RBL.CssClass = "RadioDistancia";

            RBL.Items.Add(new ListItem("Siempre", "0"));
            RBL.Items.Add(new ListItem("Casi siempre", "1"));
            RBL.Items.Add(new ListItem("Algunas veces", "2"));
            RBL.Items.Add(new ListItem("Casi nunca", "3"));
            RBL.Items.Add(new ListItem("Nunca", "4"));

            AreaPreguntas.Controls.Add(RBL);

            //HTML
            AreaPreguntas.Controls.Add(new LiteralControl("</div>"));
        }

        protected void BtnNext_Click(object sender, EventArgs e)
        {
            if (grupo <= 14)
            {
                totalpreguntas = listPreguntas.Count;

                c.UsuarioId = user;
                string[,] Respuestas = new string[totalpreguntas, 2];

                for (int i = 0; i < totalpreguntas; i++)
                {
                    Respuestas[i, 0] = listPreguntas[i].Id.ToString();

                    RadioButtonList r = (RadioButtonList)FindControl("RadioButtonList" + listPreguntas[i].Id.ToString());

                    Respuestas[i, 1] = r.SelectedValue;
                }
                c.Insert(Respuestas);
            }
            grupo++;
            if (grupo == 15)
                Response.Redirect("Resultados.aspx?us=" + user.ToString());
            else
                Response.Redirect("Cuestionario.aspx?gp=" + grupo.ToString() + "&us=" + user.ToString());
        }

        protected void BtnBack_Click(object sender, EventArgs e)
        {
            if (grupo > 1)
            {
                grupo--;
                Response.Redirect("Cuestionario.aspx?gp=" + grupo.ToString() + "&us=" + user.ToString());
            }
        }

        protected void BtnModalSi_Click(object sender, EventArgs e)
        {

        }

        protected void BtnModalNo_Click(object sender, EventArgs e)
        {
            grupo++;
            if (grupo == 15)
                Response.Redirect("Resultados.aspx?us=" + user.ToString());
            else
                Response.Redirect("Cuestionario.aspx?gp=" + grupo.ToString() + "&us=" + user.ToString());
        }
    }
}