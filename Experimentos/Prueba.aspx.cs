﻿using NOM_035.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NOM_035
{
    public partial class Prueba : System.Web.UI.Page
    {
        #region variables
        int user = 0;
        int grupo = 1;
        int totalpreguntas = 0;
        List<preguntas> listPreguntas;
        private readonly preguntas p = new preguntas();
        private readonly cuestionario c = new cuestionario();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                diverror.Visible = false;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            Llenar();
        }

        public void Llenar()
        {
            p.Searchby = grupo.ToString();
            listPreguntas = p.GetGrupo(ref lblerror, ref diverror);
            totalpreguntas = listPreguntas.Count;
            H2.Text = "Preguntas NOM - 035 - Grupo " + grupo.ToString();

            for (int i = 0; i < totalpreguntas; i++)
            {
                CreateArea(listPreguntas[i].Id.ToString(), listPreguntas[i].Pregunta);
                if (i == totalpreguntas - 1)
                {
                    //preguntas = 100 / 72; = 1.38888888889
                    double porcentage = 1.38888888889 * listPreguntas[i].Id;
                    porcentage = Math.Round(porcentage);

                    AreaProgress.Controls.Add(new LiteralControl("<div class='progress-bar' role='progressbar' style='width: " + porcentage.ToString() + "%' aria-valuenow='" + porcentage.ToString() + "' aria-valuemin='0' aria-valuemax='100'></div>"));
                }
            }
        }

        private void CreateArea(string areaCount, string pregunta)
        {
            //HTML
            AreaPreguntas.Controls.Add(new LiteralControl("<div style='margin-bottom: 15px;' class='list-group-item list-group-item-action list-group-item-info'>"));
            AreaPreguntas.Controls.Add(new LiteralControl("<div class='input-group'>"));

            //ASP
            Label Labelcito = new Label();
            Labelcito.ID = "Label" + areaCount;
            Labelcito.AssociatedControlID = "RadioButtonList" + areaCount;
            Labelcito.Text = areaCount + "." + pregunta;

            AreaPreguntas.Controls.Add(Labelcito);

            RequiredFieldValidator RFV = new RequiredFieldValidator();
            RFV.ID = "RequiredFieldValidator" + areaCount;
            RFV.ControlToValidate = "RadioButtonList" + areaCount;
            RFV.CssClass = "alert alert-danger";
            RFV.Style.Add("padding", "5px");
            RFV.Style.Add("margin", "0px 15px");
            RFV.ErrorMessage = "Por Favor Responda esta Pregunta!";

            AreaPreguntas.Controls.Add(RFV);

            //HTML
            AreaPreguntas.Controls.Add(new LiteralControl("</div>"));

            //ASP
            RadioButtonList RBL = new RadioButtonList();
            RBL.ID = "RadioButtonList" + areaCount;
            RBL.RepeatDirection = RepeatDirection.Horizontal;
            RBL.Width = 600;
            RBL.Style.Add("margin-left", "20px");
            RBL.CellPadding = 10;
            RBL.CssClass = "RadioDistancia";

            RBL.Items.Add(new ListItem("Siempre", "0"));
            RBL.Items.Add(new ListItem("Casi siempre", "1"));
            RBL.Items.Add(new ListItem("Algunas veces", "2"));
            RBL.Items.Add(new ListItem("Casi nunca", "3"));
            RBL.Items.Add(new ListItem("Nunca", "4"));

            AreaPreguntas.Controls.Add(RBL);

            //HTML
            AreaPreguntas.Controls.Add(new LiteralControl("</div>"));
        }

        protected void BtnNext_Click(object sender, EventArgs e)
        {
            if (grupo < 14)
            {
                totalpreguntas = listPreguntas.Count;

                c.UsuarioId = user;
                string[,] Respuestas = new string[totalpreguntas, 2];

                for (int i = 0; i < totalpreguntas; i++)
                {
                    Respuestas[i, 0] = listPreguntas[i].Id.ToString();

                    Control p = (Control)FindControl("Preguntas");
                    Panel p2 = (Panel)FindControl("AreaPreguntas");
                    RadioButtonList r = (RadioButtonList)FindControl("RadioButtonList" + listPreguntas[i].Id.ToString());
                    Respuestas[i, 1] = r.SelectedValue;

                    //https://sites.google.com/site/lagaterainformatica/home/-net/-net-c-/-generico/-crear-controles-de-usuario-dinamicamente-y-obtener-valores-de-sus-controles
                    //https://stackoverrun.com/es/q/8495322
                    //https://forums.asp.net/t/1186195.aspx?FAQ+Why+do+dynamic+controls+disappear+on+postback+and+not+raise+events+
                    //https://si.ua.es/es/documentacion/aplicacion-net/documentos/pdf/tema-9-persistencia-y-objetos-dinamicos.pdf
                }
                grupo++;

                Response.Redirect("Prueba.aspx?gp=" + grupo.ToString() + "&us=" + user.ToString());
            }
        }

        protected void BtnBack_Click(object sender, EventArgs e)
        {
            if (grupo > 1)
            {
                grupo--;
                Response.Redirect("Prueba.aspx?gp=" + grupo.ToString() + "&us=" + user.ToString());
            }
        }
    }
}