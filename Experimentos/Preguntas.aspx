﻿<%@ Page Title="Preguntas" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Preguntas.aspx.cs" Inherits="NOM_035.Preguntas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row page-header">
        <h3>
            <asp:Label ID="H2" runat="server" Text="Preguntas NOM-035 - Grupo 1" />
        </h3>
    </div>

    <asp:Panel ID="AreaProgress" runat="server" CssClass="row progress"></asp:Panel>

    <div class="row divmsgBox">
        <div id="diverror" runat="server" class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Oh no 😱! </strong>
            <asp:Label ID="lblerror" runat="server" Text="Hectorw/h"></asp:Label>
        </div>
    </div>

    <asp:Panel ID="AreaPreguntas" runat="server" CssClass="row list-group"></asp:Panel>

    <div class="row">
        <%-- Botones --%>
        <div class="nav navbar-right" style="margin-right: 20px">
            <asp:Button ID="BtnNext" runat="server" CssClass="btn btn-primary" Text="Siguiente" OnClick="BtnNext_Click" />
        </div>

        <div class="nav navbar-left" style="margin-left: 20px">
            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary" Text="Anterior" OnClick="BtnBack_Click" CausesValidation="False" />
        </div>
    </div>
</asp:Content>
