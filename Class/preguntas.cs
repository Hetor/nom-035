﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace NOM_035.Class
{
    public class preguntas
    {
        #region Properties
        private int _id;
        private string _pregunta;
        private int _grupo;
        private int _valor;
        private string _searchby;


        public int Id { get => _id; set => _id = value; }
        public string Pregunta { get => _pregunta; set => _pregunta = value; }
        public int Grupo { get => _grupo; set => _grupo = value; }
        public int Valor { get => _valor; set => _valor = value; }
        public string Searchby { get => _searchby; set => _searchby = value; }
        #endregion

        #region Query
        public static Tuple<List<preguntas>, string> gSearch(string sql)
        {
            string Error = "";
            List<preguntas> listpreguntas = new List<preguntas>();
            try
            {
                string constring = WebConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constring))
                {
                    SqlCommand cmd = new SqlCommand(sql, con);
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        preguntas p = new preguntas();
                        p.Id = Convert.ToInt32(dr["id"]);
                        p.Pregunta = dr["pregunta"].ToString();
                        p.Grupo = Convert.ToInt32(dr["grupo"]);
                        p.Valor = Convert.ToInt32(dr["valor"]);

                        listpreguntas.Add(p);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.ToString();
            }
            return new Tuple<List<preguntas>, string>(listpreguntas, Error);
        }

        public List<preguntas> GetGrupo(ref Label lblerror, ref HtmlGenericControl diverror)
        {
            Tuple<List<preguntas>, string> listpreguntas = gSearch("SELECT * FROM [t_pregunta] WHERE grupo = " + Searchby);
            MsgBox(ref lblerror, ref diverror, listpreguntas.Item2);
            return listpreguntas.Item1;
        }

        public List<preguntas> GetPreguntas(ref Label lblerror, ref HtmlGenericControl diverror)
        {
            Tuple<List<preguntas>, string> listpreguntas = gSearch("SELECT * FROM [t_pregunta]");
            MsgBox(ref lblerror, ref diverror, listpreguntas.Item2);
            return listpreguntas.Item1;
        }
        #endregion

        #region MsgBox
        public void MsgBox(ref Label lblerror, ref HtmlGenericControl diverror, string msg)
        {
            if (msg != "")
            {
                lblerror.Text = msg;
                diverror.Visible = true;
            }
        }
        #endregion
    }
}