﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
namespace NOM_035.Class
{
    public class cuestionario
    {
        #region Properties
        private int _id;
        private int _usuarioid;
        private int _preguntaid;
        private int _respuesta;

        public int Id { get => _id; set => _id = value; }
        public int UsuarioId { get => _usuarioid; set => _usuarioid = value; }
        public int PreguntaId { get => _preguntaid; set => _preguntaid = value; }
        public int Respuesta { get => _respuesta; set => _respuesta = value; }
        #endregion

        private static Tuple<List<cuestionario>, string> gSearch(string sql)
        {
            string Error = "";
            List<cuestionario> listcuestionario = new List<cuestionario>();
            try
            {
                string constring = WebConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constring))
                {
                    SqlCommand cmd = new SqlCommand(sql, con);
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        cuestionario u = new cuestionario();
                        u.Id = Convert.ToInt32(dr["id"]);
                        u.UsuarioId = Convert.ToInt32(dr["usuario_id"]);
                        u.PreguntaId = Convert.ToInt32(dr["pregunta_id"]);
                        u.Respuesta = Convert.ToInt32(dr["respuesta"]);

                        listcuestionario.Add(u);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.ToString();
            }
            return new Tuple<List<cuestionario>, string>(listcuestionario, Error);
        }

        public List<cuestionario> GetResultados(ref Label lblerror, ref HtmlGenericControl diverror)
        {
            Tuple<List<cuestionario>, string> listuser = gSearch("SELECT * FROM [dbo].[t_cuestionario]  WHERE [usuario_id] = '" + UsuarioId.ToString() + "'");
            MsgBox(ref lblerror, ref diverror, listuser.Item2);
            return listuser.Item1;
        }

        #region Method
        private int gMethod(string sql)
        {
            int record = 0;
            string constring = WebConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            SqlConnection con = new SqlConnection(constring);
            try
            {
                using (con)
                {
                    SqlCommand cmd = new SqlCommand(sql, con);
                    con.Open();
                    record = cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                con.Close();
            }
            return record;
        }

        public int Insert(string[,] Respuesta)
        {
            string sql = "";
            for (int i = 0; i < Respuesta.Length / 2; i++)
            {
                sql += "('" + UsuarioId.ToString() + "', '" + Respuesta[i, 0] + "', '" + Respuesta[i, 1] + "')";
                if (i >= (Respuesta.Length / 2) - 1)
                    sql += ";";
                else
                    sql += ",";
            }

            return gMethod("INSERT INTO [dbo].[t_cuestionario]  ([usuario_id],[pregunta_id],[respuesta]) VALUES " + sql);
        }
        #endregion

        #region MsgBox        
        public void MsgBox(ref Label lblerror, ref HtmlGenericControl diverror, string msg)
        {
            if (msg != "")
            {
                lblerror.Text = msg;
                diverror.Visible = true;
            }
        }
        #endregion
    }
}