﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace NOM_035.Class
{
    public class Usuarios
    {
        #region Properties
        private int _id;
        private string _nombre;
        private string apellido;
        private string empresa;
        private int _valor;
        private string _searchby;

        public int Id { get => _id; set => _id = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Empresa { get => empresa; set => empresa = value; }
        public int Valor { get => _valor; set => _valor = value; }
        public string Searchby { get => _searchby; set => _searchby = value; }
        #endregion

        #region Query
        public static Tuple<List<Usuarios>, string> gSearch(string sql)
        {
            string Error = "";
            List<Usuarios> listUsuarios = new List<Usuarios>();
            try
            {
                string constring = WebConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constring))
                {
                    SqlCommand cmd = new SqlCommand(sql, con);
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        Usuarios u = new Usuarios();
                        u.Id = Convert.ToInt32(dr["id"]);
                        u.Nombre = dr["nombre"].ToString();
                        u.Apellido = dr["apellido"].ToString();
                        u.Empresa = dr["empresa"].ToString();

                        listUsuarios.Add(u);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.ToString();
            }
            return new Tuple<List<Usuarios>, string>(listUsuarios, Error);
        }

        public List<Usuarios> GetAll(ref Label lblerror, ref HtmlGenericControl diverror)
        {
            Tuple<List<Usuarios>, string> listuser = gSearch("SELECT * FROM [t_usuario] order by empresa, nombre");
            MsgBox(ref lblerror, ref diverror, listuser.Item2);
            return listuser.Item1;
        }

        public List<Usuarios> Search(ref Label lblerror, ref HtmlGenericControl diverror)
        {
            Tuple<List<Usuarios>, string> listUsuarios = gSearch("SELECT * FROM [t_usuario] WHERE nombre LIKE '%" + Searchby + "%' OR apellido LIKE '%" + Searchby + "%' OR empresa LIKE '%" + Searchby + "%'");
            MsgBox(ref lblerror, ref diverror, listUsuarios.Item2);
            return listUsuarios.Item1;
        }

        public int OneRecord(string sql)
        {
            int record = 0;
            string constring = WebConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            SqlConnection con = new SqlConnection(constring);
            try
            {
                SqlCommand cmd = new SqlCommand(sql, con);
                con.Open();
                SqlDataReader reader;
                reader = cmd.ExecuteReader();
                reader.Read();

                Id = Convert.ToInt32(reader["id"]);
                Nombre = reader["nombre"].ToString();
                Apellido = reader["apellido"].ToString();
                Empresa = reader["empresa"].ToString();
                reader.Close();
                record = 1;
            }
            finally
            {
                con.Close();
            }
            return record;
        }
        #endregion

        #region Method
        private int gMethod(string sql)
        {
            int record = 0;
            string constring = WebConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            SqlConnection con = new SqlConnection(constring);
            try
            {
                using (con)
                {
                    SqlCommand cmd = new SqlCommand(sql, con);
                    con.Open();
                    record = cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                //error
            }
            finally
            {
                con.Close();
            }
            return record;
        }

        public int Insert()
        {
            return gMethod("INSERT INTO [t_usuario] ([nombre],[apellido],[empresa]) VALUES ('" + Nombre + "','" + Apellido + "','" + Empresa + "')");
        }

        public int GetUltimo()
        {
            return OneRecord("SELECT * FROM [t_usuario] WHERE nombre ='" + Nombre + "' AND apellido ='" + Apellido +"'");
        }

        public int GetNombre()
        {
            return OneRecord("SELECT * FROM [t_usuario] WHERE id =" + Id);
        }

        public int Update()
        {
            return gMethod("UPDATE [t_usuario] SET [nombre] = '" + Nombre + "',[apellido] = '" + Apellido + "',[empresa] = '" + Empresa + "' WHERE [id] = '" + Id + "'");
        }

        public int UpdateAdd()
        {
            return gMethod("DELETE [t_usuario] SET WHERE [id] = '" + Id + "'");
        }

        public int UpdateRemove()
        {
            return gMethod("UPDATE [t_usuario] SET  [user_status] = '0' WHERE [id_user] = '" + Id + "'");
        }
        #endregion Method

        #region MsgBox
        public void MsgBox(ref Label lblerror, ref HtmlGenericControl diverror, string msg)
        {
            if (msg != "")
            {
                lblerror.Text = msg;
                diverror.Visible = true;
            }
        }
        #endregion
    }
}