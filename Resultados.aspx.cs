﻿using NOM_035.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NOM_035
{
    public partial class Resultados : System.Web.UI.Page
    {
        #region variables
        int totalpreguntas = 0;
        List<cuestionario> listCuestionarios;
        List<preguntas> listPreguntas;
        List<Usuarios> listUsuarios;
        private readonly Usuarios u = new Usuarios();
        private readonly preguntas p = new preguntas();
        private readonly cuestionario c = new cuestionario();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["us"] != null)
            {
                Session["ID"] = Request.QueryString["us"];
                u.Id = Convert.ToInt32(Session["ID"]);
                u.GetNombre();
                Title = "REULTADOS " + u.Nombre + " " + u.Apellido;
                Nombre.Text = u.Nombre + " " + u.Apellido;
            }

            diverror.Visible = false;

            Llenar();
            Valores();
        }

        public void Llenar()
        {
            listPreguntas = p.GetPreguntas(ref lblerror, ref diverror);
            totalpreguntas = listPreguntas.Count;
            c.UsuarioId = Convert.ToInt32(Session["ID"]);
            listCuestionarios = c.GetResultados(ref lblerror, ref diverror);

            //HTML
            AreaPreguntas.Controls.Add(new LiteralControl("<table class='table table-bordered' style='width: 100 %'>"));

            for (int i = 0; i < totalpreguntas; i++)
            {
                int respuesta = -1;
                if (i < listCuestionarios.Count)
                    respuesta = listCuestionarios[i].Respuesta;

                CreateArea(listPreguntas[i].Id.ToString(), listPreguntas[i].Pregunta, respuesta);
            }

            //HTML
            AreaPreguntas.Controls.Add(new LiteralControl("</table>"));
        }

        private void CreateArea(string areaCount, string pregunta, int Respuesta)
        {
            AreaPreguntas.Controls.Add(new LiteralControl("<tr> <td>"));

            //ASP
            Label Labelcito = new Label();
            Labelcito.ID = "Label" + areaCount;
            Labelcito.Text = areaCount + "." + pregunta;

            AreaPreguntas.Controls.Add(Labelcito);

            AreaPreguntas.Controls.Add(new LiteralControl("</td> <td style='text-align:center'>"));

            //ASP
            Label Pregunta = new Label();
            Pregunta.ID = "Pregunta" + areaCount;

            if (Respuesta != -1)
            {
                switch (Respuesta)
                {
                    case 0:
                        Pregunta.Text = "⚫ Siempre          ⚪ Casi siempre          ⚪ Algunas veces          ⚪ Casi nunca          ⚪ Nunca";
                        break;
                    case 1:
                        Pregunta.Text = "⚪ Siempre          ⚫ Casi siempre          ⚪ Algunas veces          ⚪ Casi nunca          ⚪ Nunca";
                        break;
                    case 2:
                        Pregunta.Text = "⚪ Siempre          ⚪ Casi siempre          ⚫ Algunas veces          ⚪ Casi nunca          ⚪ Nunca";
                        break;
                    case 3:
                        Pregunta.Text = "⚪ Siempre          ⚪ Casi siempre          ⚪ Algunas veces          ⚫ Casi nunca          ⚪ Nunca";
                        break;
                    case 4:
                        Pregunta.Text = "⚪ Siempre          ⚪ Casi siempre          ⚪ Algunas veces          ⚪ Casi nunca          ⚫ Nunca";
                        break;
                }
            }
            else
                Pregunta.Text = "⚪ Siempre          ⚪ Casi siempre          ⚪ Algunas veces          ⚪ Casi nunca          ⚪ Nunca";

            AreaPreguntas.Controls.Add(Pregunta);

            AreaPreguntas.Controls.Add(new LiteralControl("</td></tr>"));
        }

        public void Valores()
        {
            int total = 0;
            string[] valor = new string[72];

            #region PUNTAJE
            for (int i = 0; i < listCuestionarios.Count; i++)
            {
                if (i + 1 == 1 || i + 1 == 4 || i + 1 == 23 || i + 1 == 24 || i + 1 == 25 || i + 1 == 26 || i + 1 == 27 || i + 1 == 28 || i + 1 == 30 || i + 1 == 31 || i + 1 == 32 || i + 1 == 33 || i + 1 == 34 || i + 1 == 35 || i + 1 == 36 || i + 1 == 37 || i + 1 == 38 || i + 1 == 39 || i + 1 == 40 || i + 1 == 41 || i + 1 == 42 || i + 1 == 43 || i + 1 == 44 || i + 1 == 45 || i + 1 == 46 || i + 1 == 47 || i + 1 == 48 || i + 1 == 49 || i + 1 == 50 || i + 1 == 51 || i + 1 == 52 || i + 1 == 53 || i + 1 == 55 || i + 1 == 56 || i + 1 == 57)
                {
                    valor[i] = listCuestionarios[i].Respuesta.ToString();
                }
                else if (i + 1 == 2 || i + 1 == 3 || i + 1 == 5 || i + 1 == 6 || i + 1 == 7 || i + 1 == 8 || i + 1 == 9 || i + 1 == 10 || i + 1 == 11 || i + 1 == 12 || i + 1 == 13 || i + 1 == 14 || i + 1 == 15 || i + 1 == 16 || i + 1 == 17 || i + 1 == 18 || i + 1 == 19 || i + 1 == 20 || i + 1 == 21 || i + 1 == 22 || i + 1 == 29 || i + 1 == 54 || i + 1 == 58 || i + 1 == 59 || i + 1 == 60 || i + 1 == 61 || i + 1 == 62 || i + 1 == 63 || i + 1 == 64 || i + 1 == 65 || i + 1 == 66 || i + 1 == 67 || i + 1 == 68 || i + 1 == 69 || i + 1 == 70 || i + 1 == 71 || i + 1 == 72)
                {
                    valor[i] = (-(listCuestionarios[i].Respuesta - 4)).ToString();
                }
            }

            Label1.Text = Convert.ToString(Convert.ToInt32(valor[1 - 1]) + Convert.ToInt32(valor[3 - 1]));
            Label2.Text = Convert.ToString(Convert.ToInt32(valor[2 - 1]) + Convert.ToInt32(valor[4 - 1]));
            Label3.Text = Convert.ToString(Convert.ToInt32(valor[5 - 1]));
            Label4.Text = Convert.ToString(Convert.ToInt32(valor[6 - 1]) + Convert.ToInt32(valor[12 - 1]));
            Label5.Text = Convert.ToString(Convert.ToInt32(valor[7 - 1]) + Convert.ToInt32(valor[8 - 1]));
            Label6.Text = Convert.ToString(Convert.ToInt32(valor[9 - 1]) + Convert.ToInt32(valor[10 - 1]) + Convert.ToInt32(valor[11 - 1]));
            Label7.Text = Convert.ToString(Convert.ToInt32(valor[65 - 1]) + Convert.ToInt32(valor[66 - 1]) + Convert.ToInt32(valor[67 - 1]) + Convert.ToInt32(valor[68 - 1]));
            Label8.Text = Convert.ToString(Convert.ToInt32(valor[13 - 1]) + Convert.ToInt32(valor[14 - 1]));
            Label9.Text = Convert.ToString(Convert.ToInt32(valor[15 - 1]) + Convert.ToInt32(valor[16 - 1]));
            Label10.Text = Convert.ToString(Convert.ToInt32(valor[25 - 1]) + Convert.ToInt32(valor[26 - 1]) + Convert.ToInt32(valor[27 - 1]) + Convert.ToInt32(valor[28 - 1]));
            Label11.Text = Convert.ToString(Convert.ToInt32(valor[23 - 1]) + Convert.ToInt32(valor[24 - 1]));
            Label12.Text = Convert.ToString(Convert.ToInt32(valor[29 - 1]) + Convert.ToInt32(valor[30 - 1]));
            Label13.Text = Convert.ToString(Convert.ToInt32(valor[35 - 1]) + Convert.ToInt32(valor[36 - 1]));
            Label14.Text = Convert.ToString(Convert.ToInt32(valor[17 - 1]) + Convert.ToInt32(valor[18 - 1]));
            Label15.Text = Convert.ToString(Convert.ToInt32(valor[19 - 1]) + Convert.ToInt32(valor[20 - 1]));
            Label16.Text = Convert.ToString(Convert.ToInt32(valor[21 - 1]) + Convert.ToInt32(valor[22 - 1]));
            Label17.Text = Convert.ToString(Convert.ToInt32(valor[31 - 1]) + Convert.ToInt32(valor[32 - 1]) + Convert.ToInt32(valor[33 - 1]) + Convert.ToInt32(valor[34 - 1]));
            Label18.Text = Convert.ToString(Convert.ToInt32(valor[37 - 1]) + Convert.ToInt32(valor[38 - 1]) + Convert.ToInt32(valor[39 - 1]) + Convert.ToInt32(valor[40 - 1]) + Convert.ToInt32(valor[41 - 1]));
            Label19.Text = Convert.ToString(Convert.ToInt32(valor[42 - 1]) + Convert.ToInt32(valor[43 - 1]) + Convert.ToInt32(valor[44 - 1]) + Convert.ToInt32(valor[45 - 1]) + Convert.ToInt32(valor[46 - 1]));
            Label20.Text = Convert.ToString(Convert.ToInt32(valor[69 - 1]) + Convert.ToInt32(valor[70 - 1]) + Convert.ToInt32(valor[71 - 1]) + Convert.ToInt32(valor[72 - 1]));
            Label21.Text = Convert.ToString(Convert.ToInt32(valor[57 - 1]) + Convert.ToInt32(valor[58 - 1]) + Convert.ToInt32(valor[59 - 1]) + Convert.ToInt32(valor[60 - 1]) + Convert.ToInt32(valor[61 - 1]) + Convert.ToInt32(valor[62 - 1]) + Convert.ToInt32(valor[63 - 1]) + Convert.ToInt32(valor[64 - 1]));
            Label22.Text = Convert.ToString(Convert.ToInt32(valor[47 - 1]) + Convert.ToInt32(valor[48 - 1]));
            Label23.Text = Convert.ToString(Convert.ToInt32(valor[49 - 1]) + Convert.ToInt32(valor[50 - 1]) + Convert.ToInt32(valor[51 - 1]) + Convert.ToInt32(valor[52 - 1]));
            Label24.Text = Convert.ToString(Convert.ToInt32(valor[55 - 1]) + Convert.ToInt32(valor[56 - 1]));
            Label25.Text = Convert.ToString(Convert.ToInt32(valor[53 - 1]) + Convert.ToInt32(valor[54 - 1]));
            #endregion

            #region CALIFICACIÓN FINAL DEL CUESTIONARIO
            for (int i = 0; i < valor.Length; i++)
            {
                total += Convert.ToInt32(valor[i]);
            }

            AreaRespuestas.Controls.Add(new LiteralControl("<table class='table table-bordered' style='width: 100 %'><thead><tr><th style='text-align:center; background: #eeeeee;'>Resultado del cuestionario</th><th style='text-align:center; background: #eeeeee;'>Nulo o despreciable</th><th style='text-align:center; background: #eeeeee;'>Bajo</th><th style='text-align:center; background: #eeeeee;'>Medio</th><th style='text-align:center; background: #eeeeee;'>Alto</th><th style='text-align:center; background: #eeeeee;'>Muy alto</th></tr></thead><tbody><tr>"));
            if (total < 50)
                AreaRespuestas.Controls.Add(new LiteralControl("<td style='text-align:center'>" + total + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (50 <= total && total < 75)
                AreaRespuestas.Controls.Add(new LiteralControl("<td style='text-align:center'>" + total + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (75 <= total && total < 99)
                AreaRespuestas.Controls.Add(new LiteralControl("<td style='text-align:center'>" + total + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (99 <= total && total < 140)
                AreaRespuestas.Controls.Add(new LiteralControl("<td style='text-align:center'>" + total + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (total >= 140)
                AreaRespuestas.Controls.Add(new LiteralControl("<td style='text-align:center'>" + total + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr></tbody></table>"));

            AreaRespuestas.Controls.Add(new LiteralControl("</br>"));
            #endregion

            #region CALIFICACIÓN FINAL POR CATEGORIA
            int uno = Convert.ToInt32(Label1.Text) + Convert.ToInt32(Label2.Text) + Convert.ToInt32(Label3.Text);
            int dos = Convert.ToInt32(Label4.Text) + Convert.ToInt32(Label5.Text) + Convert.ToInt32(Label6.Text) + Convert.ToInt32(Label7.Text) + Convert.ToInt32(Label8.Text) + Convert.ToInt32(Label9.Text) + Convert.ToInt32(Label10.Text) + Convert.ToInt32(Label11.Text) + Convert.ToInt32(Label12.Text) + Convert.ToInt32(Label13.Text);
            int tres = Convert.ToInt32(Label14.Text) + Convert.ToInt32(Label15.Text) + Convert.ToInt32(Label16.Text);
            int cuatro = Convert.ToInt32(Label17.Text) + Convert.ToInt32(Label18.Text) + Convert.ToInt32(Label19.Text) + Convert.ToInt32(Label20.Text) + Convert.ToInt32(Label21.Text);
            int cinco = Convert.ToInt32(Label22.Text) + Convert.ToInt32(Label23.Text) + Convert.ToInt32(Label24.Text) + Convert.ToInt32(Label25.Text);

            AreaRespuestas.Controls.Add(new LiteralControl("<table class='table table-bordered' style='width: 100 %'><thead><tr><th style='text-align:center; background: #eeeeee;'>Calificación de la categoría</th><th style='text-align:center; background: #eeeeee;'>Nulo o despreciable</th><th style='text-align:center; background: #eeeeee;'>Bajo</th><th style='text-align:center; background: #eeeeee;'>Medio</th><th style='text-align:center; background: #eeeeee;'>Alto</th><th style='text-align:center; background: #eeeeee;'>Muy alto</th></tr></thead><tbody>"));

            //Ambiente de trabajo
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (uno < 5)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Ambiente de trabajo " + uno.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (5 <= uno && uno < 9)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Ambiente de trabajo " + uno.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (9 <= uno && uno < 11)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Ambiente de trabajo " + uno.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (11 <= uno && uno < 14)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Ambiente de trabajo " + uno.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (uno >= 14)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Ambiente de trabajo " + uno.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Factores propios de la actividad
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (dos < 15)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Factores propios de la actividad " + dos.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (15 <= dos && dos < 30)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Factores propios de la actividad " + dos.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (30 <= dos && dos < 45)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Factores propios de la actividad " + dos.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (45 <= dos && dos < 60)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Factores propios de la actividad " + dos.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (dos >= 60)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Factores propios de la actividad " + dos.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Organización del tiempo de trabajo
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (tres < 5)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Organización del tiempo de trabajo " + tres.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (5 <= tres && tres < 7)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Organización del tiempo de trabajo" + tres.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (7 <= tres && tres < 10)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Organización del tiempo de trabajo" + tres.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (10 <= tres && tres < 13)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Organización del tiempo de trabajo" + tres.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (tres >= 13)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Organización del tiempo de trabajo" + tres.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Liderazgo y relaciones en el trabajo
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (cuatro < 14)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Liderazgo y relaciones en el trabajo " + cuatro.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (14 <= cuatro && cuatro < 29)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Liderazgo y relaciones en el trabajo " + cuatro.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (29 <= cuatro && cuatro < 42)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Liderazgo y relaciones en el trabajo " + cuatro.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (42 <= cuatro && cuatro < 58)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Liderazgo y relaciones en el trabajo " + cuatro.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (cuatro >= 58)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Liderazgo y relaciones en el trabajo " + cuatro.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Entorno organizacional
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (cinco < 10)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Entorno organizacional " + cinco.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (10 <= cinco && cinco < 14)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Entorno organizacional " + cinco.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (14 <= cinco && cinco < 18)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Entorno organizacional " + cinco.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (18 <= cinco && cinco < 23)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Entorno organizacional " + cinco.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (cinco >= 23)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Entorno organizacional " + cinco.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr></tbody></table>"));

            AreaRespuestas.Controls.Add(new LiteralControl("</br>"));
            #endregion

            #region CALIFICACIÓN FINAL POR DOMINIO
            int one = Convert.ToInt32(Label1.Text) + Convert.ToInt32(Label2.Text) + Convert.ToInt32(Label3.Text);
            int two = Convert.ToInt32(Label4.Text) + Convert.ToInt32(Label5.Text) + Convert.ToInt32(Label6.Text) + Convert.ToInt32(Label7.Text) + Convert.ToInt32(Label8.Text) + Convert.ToInt32(Label9.Text);
            int three = Convert.ToInt32(Label10.Text) + Convert.ToInt32(Label11.Text) + Convert.ToInt32(Label12.Text) + Convert.ToInt32(Label13.Text);
            int four = Convert.ToInt32(Label14.Text);
            int five = Convert.ToInt32(Label15.Text) + Convert.ToInt32(Label16.Text);
            int six = Convert.ToInt32(Label17.Text) + Convert.ToInt32(Label18.Text);
            int seven = Convert.ToInt32(Label19.Text) + Convert.ToInt32(Label20.Text);
            int eight = Convert.ToInt32(Label21.Text);
            int nine = Convert.ToInt32(Label22.Text) + Convert.ToInt32(Label23.Text);
            int ten = Convert.ToInt32(Label24.Text) + Convert.ToInt32(Label25.Text);

            AreaRespuestas.Controls.Add(new LiteralControl("<table class='table table-bordered' style='width: 100 %'><thead><tr><th style='text-align:center; background: #eeeeee;'>Calificación de la categoría</th><th style='text-align:center; background: #eeeeee;'>Nulo o despreciable</th><th style='text-align:center; background: #eeeeee;'>Bajo</th><th style='text-align:center; background: #eeeeee;'>Medio</th><th style='text-align:center; background: #eeeeee;'>Alto</th><th style='text-align:center; background: #eeeeee;'>Muy alto</th></tr></thead><tbody>"));

            //Condiciones en el ambiente de trabajo
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (one < 5)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Condiciones en el ambiente de trabajo " + one.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (5 <= one && one < 9)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Condiciones en el ambiente de trabajo " + one.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (9 <= one && one < 11)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Condiciones en el ambiente de trabajo " + one.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (11 <= one && one < 14)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Condiciones en el ambiente de trabajo " + one.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (one >= 14)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Condiciones en el ambiente de trabajo " + one.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Carga de trabajo
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (two < 15)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Carga de trabajo " + two.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (15 <= two && two < 21)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Carga de trabajo " + two.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (21 <= two && two < 27)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Carga de trabajo " + two.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (27 <= two && two < 37)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Carga de trabajo " + two.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (two >= 37)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Carga de trabajo " + two.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Organización del tiempo de trabajo
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (three < 11)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Falta de control sobre el trabajo " + three.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (11 <= three && three < 16)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Falta de control sobre el trabajo " + three.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (16 <= three && three < 21)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Falta de control sobre el trabajo " + three.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (21 <= three && three < 25)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Falta de control sobre el trabajo " + three.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (three >= 25)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Falta de control sobre el trabajo " + three.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Jornada de trabajo
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (four < 1)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Jornada de trabajo " + four.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (1 <= four && four < 2)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Jornada de trabajo " + four.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (2 <= four && four < 4)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Jornada de trabajo " + four.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (4 <= four && four < 6)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Jornada de trabajo " + four.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (four >= 6)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Jornada de trabajo " + four.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Interferencia en la relación trabajo-familia
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (five < 4)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Interferencia en la relación trabajo-familia " + five.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (4 <= five && five < 6)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Interferencia en la relación trabajo-familia " + five.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (6 <= five && five < 8)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Interferencia en la relación trabajo-familia " + five.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (8 <= five && five < 10)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Interferencia en la relación trabajo-familia " + five.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (five >= 10)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Interferencia en la relación trabajo-familia " + five.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Liderazgo
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (six < 9)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Liderazgo " + six.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (9 <= six && six < 12)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Liderazgo " + six.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (12 <= six && six < 16)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Liderazgo " + six.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (16 <= six && six < 20)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Liderazgo " + six.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (six >= 20)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Liderazgo " + six.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Relaciones en el trabajo
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (seven < 10)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Relaciones en el trabajo " + seven.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (10 <= seven && seven < 13)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Relaciones en el trabajo " + seven.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (13 <= seven && seven < 17)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Relaciones en el trabajo " + seven.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (17 <= seven && seven < 21)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Relaciones en el trabajo " + seven.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (seven >= 21)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Relaciones en el trabajo " + seven.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Violencia
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (eight < 7)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Violencia " + eight.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (7 <= eight && eight < 10)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Violencia " + eight.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (10 <= eight && eight < 13)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Violencia " + eight.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (13 <= eight && eight < 16)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Violencia " + eight.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (eight >= 16)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Violencia " + eight.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Reconocimiento del desempeño
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (nine < 6)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Reconocimiento del desempeño " + nine.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (6 <= nine && nine < 10)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Reconocimiento del desempeño " + nine.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (10 <= nine && nine < 14)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Reconocimiento del desempeño " + nine.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (14 <= nine && nine < 18)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Reconocimiento del desempeño " + nine.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (nine >= 18)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Reconocimiento del desempeño " + nine.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr>"));

            //Insuficiente sentido de pertenencia e, inestabilidad
            AreaRespuestas.Controls.Add(new LiteralControl("<tr>"));
            if (ten < 4)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Insuficiente sentido de pertenencia e, inestabilidad " + ten.ToString() + "</td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (4 <= ten && ten < 6)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Insuficiente sentido de pertenencia e, inestabilidad " + ten.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (6 <= ten && ten < 8)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Insuficiente sentido de pertenencia e, inestabilidad " + ten.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td> <td style='text-align:center'></td>"));
            if (8 <= ten && ten < 10)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Insuficiente sentido de pertenencia e, inestabilidad " + ten.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td> <td style='text-align:center'></td>"));
            if (ten >= 10)
                AreaRespuestas.Controls.Add(new LiteralControl("<td>Insuficiente sentido de pertenencia e, inestabilidad " + ten.ToString() + "</td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'></td> <td style='text-align:center'>⚫</td>"));
            AreaRespuestas.Controls.Add(new LiteralControl("</tr></tbody></table>"));


            #endregion
        }
    }
}