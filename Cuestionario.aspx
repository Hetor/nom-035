﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cuestionario.aspx.cs" Inherits="NOM_035.Cuestionario" %>

<!DOCTYPE html>

<html lang="es">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>PREGUNTAS - NOM-035.</title>

    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>

    <webopt:BundleReference runat="server" Path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />

</head>
<body>
    <form runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see https://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <img src="Other/Hungaros.png" class="navbar-brand" />
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a runat="server" href="~/">Registro</a></li>
                        <li><a runat="server" href="~/Empleados">Resultados</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container body-content">
            <%-- ------------------------------------------------------------------------------------------------------------------------------------ --%>
            <div class="row page-header">
                <h4>
                    <asp:Label ID="H2" runat="server" Text="Preguntas NOM-035 - Grupo 1" />
                </h4>
            </div>

            <asp:Panel ID="AreaProgress" runat="server" CssClass="row progress"></asp:Panel>

            <div class="row divmsgBox">
                <div id="diverror" runat="server" class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Oh no 😱! </strong>
                    <asp:Label ID="lblerror" runat="server" Text="Hectorw/h"></asp:Label>
                </div>
            </div>

            <asp:Panel ID="AreaPreguntas" runat="server" CssClass="row list-group"></asp:Panel>

            <div class="row">
                <%-- Botones --%>
                <div class="nav navbar-right" style="margin-right: 20px">
                    <asp:Button ID="BtnNext" runat="server" CssClass="btn btn-primary" Text="Siguiente" OnClick="BtnNext_Click" />
                </div>

                <div class="nav navbar-left" style="margin-left: 20px">
                    <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary" Text="Anterior" OnClick="BtnBack_Click" CausesValidation="False" Visible="False" />
                </div>
                <div class="input-group-btn">
                    <asp:LinkButton ID="btnAddresp" runat="server" class="btn btn-primary" CausesValidation="False" data-toggle="modal" data-target="#myModal" Visible="False"></asp:LinkButton>
                </div>
            </div>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <asp:Label ID="lblPregunta" runat="server" Text="En mi trabajo debo brindar servicio a clientes o usuarios?"></asp:Label>
                            <br />
                            <br />
                            <div style="text-align: center">
                                <asp:Button ID="btnModalSi" runat="server" class="btn btn-primary" Text="SI" data-dismiss="modal" ValidationGroup="modal" UseSubmitBehavior="False" OnClick="BtnModalSi_Click" />
                                <asp:Button ID="btnModalNo" runat="server" class="btn btn-danger" Text="NO" data-dismiss="modal" ValidationGroup="modal" UseSubmitBehavior="False" OnClick="BtnModalNo_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%-- ------------------------------------------------------------------------------------------------------------------------------------ --%>
            <hr />
            <footer>
                <p>Hungaros transportistas <%: DateTime.Now.Year %></p>
            </footer>
        </div>
    </form>
</body>
</html>
