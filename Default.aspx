﻿<%@ Page Title="REGISTRO" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="NOM_035._Default" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-xs-3">
        </div>
        <div class="col-lg-6 col-md-6 col-xs-6">
            <h2 style="text-align: center">
                <%: Title %>
            </h2>
            <hr />
            <div id="diverror" runat="server" class="alert alert-danger" style="margin-top: 5px;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Oh snap! </strong>
                <asp:Label ID="lblerror" runat="server" Text="Hectorw/h"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-xs-4">
        </div>
        <div class="col-lg-4 col-md-4 col-xs-4">
            <div style="margin-bottom: 15px;">
                <asp:Label ID="Label1" runat="server" AssociatedControlID="EmpresaDrop">EMPRESA</asp:Label>
                <asp:DropDownList ID="EmpresaDrop" CssClass="form-control text-uppercase" runat="server" AppendDataBoundItems="True" AutoPostBack="True">
                    <asp:ListItem Text="HUNGAROS" Value="HUNGAROS" />
                    <asp:ListItem Text="4 MATILDES" Value="4 MATILDES" />
                    <asp:ListItem Text="JAMAMADI" Value="JAMAMADI" />
                </asp:DropDownList>
            </div>
            <div style="margin-bottom: 15px;">
                <asp:Label ID="Label2" runat="server" AssociatedControlID="txtNombre">NOMBRE</asp:Label>
                <div class="input-group">
                    <asp:TextBox runat="server" ID="txtNombre" AutoPostBack="false" CssClass="form-control text-uppercase" Style="width: 280px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNombre"
                        CssClass="alert alert-danger" Style="padding: 5px; margin: 0px 15px; position: fixed;"
                        ErrorMessage="Nombre Requerido!" />
                </div>
            </div>
            <div style="margin-bottom: 15px;">
                <asp:Label ID="Label3" runat="server" AssociatedControlID="txtApellido">APELLIDO</asp:Label>
                <div class="input-group">
                    <asp:TextBox runat="server" ID="txtApellido" AutoPostBack="false" CssClass="form-control text-uppercase" Style="width: 280px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtApellido"
                        CssClass="alert alert-danger" Style="padding: 5px; margin: 0px 15px; position: fixed;"
                        ErrorMessage="Apellido Requerido!" />
                </div>
            </div>
            <div class="nav navbar-left" style="margin-right: 20px;">
                <asp:Button ID="btnCancelar" runat="server" CssClass="btn btn-danger" Text="Cancel" CausesValidation="False" OnClick="BtnCancelar_Click" />
                <asp:Button ID="btnIniciar" runat="server" CssClass="btn btn-primary" Text="Iniciar" OnClick="BtnIniciar_Click" />
            </div>
        </div>
    </div>
</asp:Content>

